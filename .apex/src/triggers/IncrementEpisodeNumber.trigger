trigger IncrementEpisodeNumber on Episode__c (after insert) {
	List<Episode__c> episodeList = new List<Episode__c>();
	List<Season__c> seasonList = [SELECT Id, Name, Number_of_Episodes__c FROM Season__c];

	for(Episode__c episod : trigger.new){
		episodeList.add(episod);
	}

	List<Episode__c> updateSeason = new List<Episode__c>([SELECT id, Episode_Number__c, Season__c FROM Episode__c WHERE Id in:episodeList]);

	for(Season__c season: seasonList){
		for(Episode__c e : updateSeason){
			if(season.Id == e.Season__c){
				e.Episode_Number__c = season.Number_of_Episodes__c+1;
			}
		}
	}
 	update updateSeason;
}