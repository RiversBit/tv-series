/*
 * Created by wojciech.kucharek on 23.10.2018.
 */
public class EditTVSerieAttachmentController {

	private final TV_Serie_Attachments__c tvSerieAttachment {get; set; }
	public String name {get; set; }
	public String attType {get; set; }

	public EditTVSerieAttachmentController(ApexPages.StandardController stdController){
		this.tvSerieAttachment = (TV_Serie_Attachments__c)stdController.getRecord();
		TV_Serie_Attachments__c defaultValue = [SELECT Id,Name, Type__c, TV_Serie__r.Id From TV_Serie_Attachments__c WHERE Id =: tvSerieAttachment.ID];
		name = defaultValue.Name;
		attType = defaultValue.Type__c;
	}

	public List<SelectOption> getAttTypes() {
		Schema.DescribeFieldResult describeResult = TV_Serie_Attachments__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
		List<SelectOption> optionsToReturn = new List<SelectOption>();

		for (Schema.PicklistEntry pEntry : entries){
			if(pEntry.isActive()) {
				optionsToReturn.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
			}
		}
		return optionsToReturn;
	}

	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public PageReference modify(){
		TV_Serie_Attachments__c parent = [SELECT TV_Serie__r.Id FROM TV_Serie_Attachments__c WHERE Id =: tvSerieAttachment.Id];
		Attachment[] doomedAtta = [SELECT Id From Attachment WHERE ParentId =: tvSerieAttachment.Id];

		try	{
			delete doomedAtta;
		}
		catch (DmlException e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Old Attachment dont delete'));
			return null;
		}

		try {
			tvSerieAttachment.Name = name;
			tvSerieAttachment.Type__c = attType;
			update tvSerieAttachment;
			attachment.OwnerId = UserInfo.getUserId();
			attachment.ParentId = tvSerieAttachment.Id;
			attachment.IsPrivate = false;
			insert attachment;
			return null;
		}
		catch (DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error uploading attachment'));
			return null;
		}
		finally {
			attachment = new Attachment();
			PageReference bactToTVSerie = new PageReference('/'+parent.TV_Serie__c);
			return bactToTVSerie;
		}
		return  null;
	}
}