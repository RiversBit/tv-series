/*
 * Created by wojciech.kucharek on 19.10.2018.
 */
public class NewTVSeriesPageController {

	private final TV_Serie__c tv_serie {get; set; }

	public NewTVSeriesPageController(ApexPages.StandardController stdController) {
		this.tv_serie = (TV_Serie__c)stdController.getRecord();
	}

	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public PageReference upload() {
		attachment.OwnerId = UserInfo.getUserId();
		attachment.ParentId = tv_serie.Id;
		attachment.IsPrivate = false;
		attachment.ContentType = 'poster';

		try{
			insert attachment;
		}
		catch (DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error uploading attachment'));
			return null;
		}
		finally {
			attachment = new Attachment();
		}

		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Attachment upload successfully'));
		return null;
	}
}
