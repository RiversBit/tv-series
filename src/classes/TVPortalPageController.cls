/*
 * Created by wojciech.kucharek on 24.10.2018.
 */
public class TVPortalPageController {

	private final TV_PORTAL__c tvPortal {get; set; }
	public List<TV_Serie__c> tvSeries {get; set; }
	public String selectedTvSeire {get; set; }

	public TVPortalPageController(ApexPages.StandardController stdController){
		this.tvPortal = (TV_PORTAL__c)stdController.getRecord();
		tvSeries = [SELECT ID, Name, Title__c From TV_Serie__c];
	}

	public PageReference viewTvSerie(){
		TV_Serie__c tvS = [SELECT ID FROM TV_Serie__c WHERE Id=:selectedTvSeire];
		PageReference viewTvSerie = new PageReference('/'+selectedTvSeire);
		return viewTvSerie;
	}

	public PageReference newTvSerie(){
		PageReference createTvSerie = new PageReference('/a00/e?retURL=%2Fa');
		createTvSerie.setRedirect(true);
		return createTvSerie;
	}
}