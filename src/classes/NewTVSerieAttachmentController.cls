/*
 * Created by wojciech.kucharek on 22.10.2018.
 */
public class NewTVSerieAttachmentController {

	private final TV_Serie_Attachments__c tvSerieAttachment {get; set; }
	public String name {get; set; }
	public String attType {get; set; }

	public NewTVSerieAttachmentController(ApexPages.StandardController stdController){
		this.tvSerieAttachment = (TV_Serie_Attachments__c)stdController.getRecord();
	}

	public List<SelectOption> getAttTypes() {
		Schema.DescribeFieldResult describeResult = TV_Serie_Attachments__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
		List<SelectOption> optionsToReturn = new List<SelectOption>();

		for (Schema.PicklistEntry pEntry : entries){
			if(pEntry.isActive()) {
				optionsToReturn.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
			}
		}
		return optionsToReturn;
	}

	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public PageReference upload(){
		System.debug(tvSerieAttachment.TV_Serie__c = ' <<<<<<<');
		try {
			tvSerieAttachment.Name = name;
			tvSerieAttachment.Type__c = attType;
			insert tvSerieAttachment;
			attachment.OwnerId = UserInfo.getUserId();
			attachment.ParentId = tvSerieAttachment.Id;
			attachment.IsPrivate = false;
			insert attachment;
			return  new PageReference('/'+tvSerieAttachment.TV_Serie__c);
		}
		catch (DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error uploading attachment'));
			return null;
		}
		finally {
			attachment = new Attachment();
		}
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Attachment upload successfully'));
		return  null;
	}
}