/*
 * Created by wojciech.kucharek on 15.10.2018.
 */
public class TVSeriesPageController {

	private final TV_Serie__c tv_serie ;
	private String season;
	public  Season__c  selectedSeason {get; set; }
	public List<Episode__c> selectedEpisodes {get; set; }
	public List<TV_Serie_Attachments__c> tvSerieAttachments {get; set;}
	public String selectedAttachment;// {get; set; }
	public String youtubeURL {get; set; }
	public Attachment logoTVSeries {get;set; }
	public List<String> screensList {get; set;}

	public TVSeriesPageController(ApexPages.StandardController stdController) {
		this.tv_serie = (TV_Serie__c)stdController.getRecord();

		List<TV_Serie__c> tempValue = [SELECT YouTube_URL__c FROM TV_Serie__c WHERE Id=:tv_serie.Id];
		youtubeURL = tempValue[0].YouTube_URL__c;

		selectedEpisodes = new List<Episode__c>();
		tvSerieAttachments = [SELECT Id, Name, Type__c, TV_Serie__r.Id FROM TV_Serie_Attachments__c WHERE TV_Serie__r.Id =: tv_serie.ID ];

		for(TV_Serie_Attachments__c logo : tvSerieAttachments){
			if(logo.Type__c == 'Logo')
				logoTVSeries = [SELECT ID FROM Attachment WHERE ParentId =: logo.Id];
		}

		screensList = new List<String>();
		for(TV_Serie_Attachments__c screen : tvSerieAttachments){

			if(screen.Type__c == 'Screen') {
				Attachment tempScreen = [SELECT ID, Name, Body FROM Attachment WHERE ParentId =: screen.ID];
				screensList.add(tempScreen.Id);
			}
		}
	}

	public String getSelectedAttachment(){
		return selectedAttachment;
	}

	public void setSelectedAttachment(string value){
		this.selectedAttachment = value;
	}

	public  TV_Serie__c getTV_Serie(){
		return tv_serie;
	}

	public String  getSeason() {
		return season;
	}

	public void setSeason(String season){
		this.season = season;
	}

	public List<SelectOption> getItems() {
		List<SelectOption> options = new List<SelectOption>();
		List<Season__c> seasons = [SELECT Id, Name, Season_Number__c FROM Season__c WHERE TV_Serie__c =: ApexPages.currentPage().getParameters().get('Id') ORDER BY Season_Number__c];

		for (Season__c s : seasons){

			options.add(new SelectOption(s.Id, String.valueOf(s.Season_Number__c))); // s.Season_Number__c));
		}
		return  options;
	}

	public List<Episode__c> getEpisodes = new List<Episode__c>();

	public PageReference test(){

		selectedSeason = [SELECT Name, Id, Season_Number__c FROM Season__c WHERE Id  =: season];
		selectedEpisodes = [SELECT ID, Name, Title__c, Episode_Number__c FROM Episode__c WHERE Season__r.Id =: season ];
		return null;
	}

	public String getFileId(){
		String fileId='';
		List<Attachment> attachedFiles = [SELECT Id FROM Attachment WHERE parent.Id =: tv_serie.Id order by LastModifiedDate DESC limit 1];
		if(attachedFiles != null && attachedFiles.size() >0){
			fileId = attachedFiles[0].Id;
		}
		return fileId;
	}

	public PageReference modifyAttachment(){
		PageReference editAttachmentPage = new PageReference('/'+selectedAttachment+'/e?retURL=%2Fa'+selectedAttachment);
		editAttachmentPage.setRedirect(true);
		return editAttachmentPage;
	}

	public PageReference deleteAttachment(){
		TV_Serie_Attachments__c doomedAttachment = [SELECT id From TV_Serie_Attachments__c WHERE id =:selectedAttachment];

		try{
			delete doomedAttachment;
		}
		catch (DmlException e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment dont delete'));
			return null;
		}

		PageReference editAttachmentPage = new PageReference('/'+tv_serie.Id);
		editAttachmentPage.setRedirect(true);
		return editAttachmentPage;
	}

	public PageReference viewAttachment(){
		Attachment atta = [SELECT Id From Attachment WHERE ParentId =:selectedAttachment];
		PageReference viewAttaPage = new PageReference('/servlet/servlet.FileDownload?file='+atta.Id);
		viewAttaPage.setRedirect(true);
		return viewAttaPage;
	}

	public PageReference newTvSerieAttachment(){
		TV_Serie_Attachments__c tv_serie_attachments = new TV_Serie_Attachments__c();
		tv_serie_attachments.TV_Serie__c = tv_serie.Id;
		insert tv_serie_attachments;
		PageReference tvAddAtta = new PageReference('/'+tv_serie_attachments.Id+'/e?retURL=%2Fa');
		return tvAddAtta;
	}

	ApexPages.StandardController stdCon;
	public PageReference addSeason(){

		Season__c season = new Season__c();
		PageReference newSeason = new PageReference('/a01/e?');
		newSeason.setRedirect(true);
		newSeason.getParameters().put('seasonNumber', '77');
		newSeason.getParameters().put( 'serieID', tv_serie.ID);

		return newSeason;

	}
}