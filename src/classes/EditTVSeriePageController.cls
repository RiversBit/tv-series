/*
 * Created by wojciech.kucharek on 24.10.2018.
 */
public class EditTVSeriePageController {
	private final TV_Serie__c tv_serie {get; set; }

	public EditTVSeriePageController(ApexPages.StandardController stdController) {
		this.tv_serie = (TV_Serie__c)stdController.getRecord();
	}

	public PageReference modify() {
		try {
			update tv_serie;
			PageReference modifyTvSerie = new PageReference('/' + tv_serie.Id);
			return modifyTvSerie;
		}
		catch (DmlException de){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, de.getDmlMessage(0)));
			return  null;
		}
		catch (Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
			return null;
		}
	}
}