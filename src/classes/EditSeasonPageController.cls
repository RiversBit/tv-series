/*
 * Created by wojciech.kucharek on 26.10.2018.
 */
public class EditSeasonPageController {

	private final Season__c season {get; set; }


	public EditSeasonPageController(ApexPages.StandardController stdController){
		this.season = (Season__c)stdController.getRecord();
		season.Season_Number__c = Decimal.valueOf(ApexPages.currentPage().getParameters().get('seasonNumber'));

		season.TV_Serie__c = ApexPages.currentPage().getParameters().get('serieID');
	}
}