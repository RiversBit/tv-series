trigger IncremenetSeasonNumber on Season__c (after insert) {
	List<Season__c> seasonsList = new List<Season__c>();
	List <TV_Serie__c> tvSeriesList = [	SELECT Id, Name, Number_of_Seasons__c From TV_Serie__c	];

	for(Season__c season : trigger.new){
		seasonsList.add(season);
	}

	List<Season__c> updateSeason = new List<Season__c>([SELECT id, Season_Number__c, TV_Serie__c FROM Season__c WHERE id in:seasonsList]);

	for(TV_Serie__c tvSerie : tvSeriesList){
		for (Season__c s : updateSeason){

			if(tvSerie.Id == s.TV_Serie__c){
				s.Season_Number__c = tvSerie.Number_of_Seasons__c+1;
			}
		}
	}
	update updateSeason;
}